title='Введите число от 1 до 12'
print(title)
month_number = int(input())

def validate_number(number):
    if number >= 1 and number <= 12:
        seasons = check_seasons(number)
        print(seasons)
    else:
        print('Вы ввели не верное число. Введите чило от 1 до 12.')
        number = int(input())
        validate_number(number)


def check_seasons(month):
    if month <= 2 or month == 12:
        return 'winter'
    if month <= 5:
        return 'spring'
    if month <= 8:
        return 'summer'
    if month <= 11:
        return 'autumn'

validate_number(month_number)
